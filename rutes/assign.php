<?php session_start() ?>
<?php require "./../components/connection.php" ?>
<?php
if (!isset($_SESSION['name'])) {
    header("Location: signin.php");
}

if (isset($_POST['assign'])) {
    $id = $_GET['id'];
    $id_user = $_POST['id_user'];
    if (!empty($id_user)) {
        foreach ($id_user as $user) {
            $mysqli->query("INSERT INTO user_projects (id, id_user) VALUES ($id, $user)");
        }
    }
}
if (isset($_POST['delete_assign'])) {
    $id = $_GET['id'];
    $id_users = $_POST['id_users'];
    if (!empty($id_users)) {
        foreach ($id_users as $users) {
            $mysqli->query("DELETE FROM user_projects WHERE id_user= '$users'");
        }
    }
}
?>
<?php require "./../components/header.php" ?>

<?php
$res = $mysqli->query("SELECT * FROM users WHERE rol = 'dev'");
$id_project = $_GET['id'];
$response = $mysqli->query("SELECT * FROM users JOIN user_projects ON user_projects.id_user = users.id_user JOIN projects ON projects.id = user_projects.id WHERE user_projects.id='$id_project'");
?>
<main>
    <div class="assign-container">
        <div class="not-assigned">
            <form method="POST">
                <h2>Not-assigned</h2>
                <?php
                foreach ($res as $result) {
                    ?>
                    <div>

                        <input type="checkbox" id="foo" name="id_user[]" value="<?= $result['id_user'] ?>">
                        <label for="foo"><?= $result['first_name'] . " " . $result['last_name'] ?></label>
                    </div>
                    <?php
                }
                ?>
                <input type="submit" name="assign" id="assign" value="Assign">
            </form>
        </div>
        <div class="assigned">
            <h2>Assigned</h2>
            <form method="POST">
                <?php
                foreach ($response as $resp) {
                    ?>
                    <div>
                        <input type="checkbox" id="foo" name="id_users[]" value="<?= $resp['id_user'] ?>">
                        <label for="foo"><?= $resp['first_name'] . " " . $resp['last_name'] ?></label>
                    </div>
                    <?php
                }
                ?>
                <input type="submit" name="delete_assign" id="delete" value="Delete">
            </form>
        </div>
    </div>
    <div class="back"><a href="index.php"><< Return</a></div>
</main>



<script>
<?php
$rows_not_assigned = $res->num_rows;
$rows_assigned = $response->num_rows;

if(!$rows_not_assigned){
?>
document.getElementById('assign').style.display = 'none'
<?php
}
if(!$rows_assigned){
    ?>
document.getElementById('delete').style.display = 'none'
<?php
}
?>
</script>
<?php require "./../components/footer.php" ?>





