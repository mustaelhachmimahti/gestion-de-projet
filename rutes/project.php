<?php session_start() ?>
<?php require "./../components/connection.php" ?>


<?php require "./../components/header.php" ?>
<?php
if (!isset($_SESSION['name'])) {
    header("Location: signin.php");
}
?>
<?php
if (isset($_POST['edit'])) {
    $new_title = $_POST['title_text'];
    $new_description = $_POST['description_text'];
    $id = $_GET['id_unic'];
    $mysqli->query("UPDATE projects SET title='$new_title'WHERE id ='$id'");
    $mysqli->query("UPDATE projects SET description='$new_description'WHERE id ='$id'");
}

$id = $_GET['id_unic'];
$res = $mysqli->query("SELECT * FROM projects WHERE id='$id'");
$project = mysqli_fetch_assoc($res);
if ($_SESSION['rol'] === "client" || $_SESSION['rol'] === "dev") {
    $id_p = $_GET['id_unic'];
    $response = $mysqli->query("SELECT * FROM users JOIN user_projects ON user_projects.id_user = users.id_user JOIN projects ON projects.id = user_projects.id WHERE user_projects.id='$id_p'");
    ?>
    <h3>People assigned to this project</h3>
    <?php
    foreach ($response as $result) {
        ?>

        <p><?= $result['first_name'] . " " . $result['last_name'] ?></p>
        <?php
    }

}
?>


    <main>
        <div class="unic-project">
            <div class="info">
                <form method="post">
                    <h2><?= $project['title'] ?></h2>
                    <p><?= $project['description'] ?></p>
                </form>
            </div>
        </div>
        <div class="edit-content">
            <form method="POST" class="modify-project">
                <input type="submit" name="edit_title" value="Modify">
            </form>
        </div>
        <div class="back"><a href="./index.php"><< Return</a></div>
    </main>


    <script>
        <?php
        if(isset($_POST['edit_title'])){
        $title = $project['title'];
        $description = $project['description'];
        ?>
        const title = document.querySelector(".info h2")
        title.innerHTML = "<input type='text' name='title_text' value='<?= $title ?>'>"
        const description = document.querySelector(".info p")
        description.innerHTML = innerHTML = "<textarea name='description_text' style='width: 100%; height: 200px;'><?= $description ?></textarea><input type='submit' value='Edit it' name='edit'><input type='submit' value='Cancel'>";
        <?php
        }
        if($_SESSION['rol'] === "client" || $_SESSION['rol'] === "dev"){
        ?>
        const modif = document.querySelector('.modify-project')
        modif.style.display = "none"
        <?php
        }

        ?>
    </script>

<?php require "./../components/footer.php" ?>