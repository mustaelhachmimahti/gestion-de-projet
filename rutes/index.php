<?php session_start() ?>
<?php include "./../components/connection.php" ?>
<?php include "./../components/header.php" ?>

<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $mysqli->query("DELETE FROM projects WHERE id='$id'");
    $mysqli->query("DELETE FROM user_projects WHERE id='$id'");
    header("Location: index.php");
}
if (isset($_POST['modif_title'])) {
    $new_title = $_POST['new_title'];
    $id = $_GET['mod'];
    $mysqli->query("UPDATE projects SET title='$new_title'WHERE id ='$id'");
    header("Location: index.php");
}
if (isset($_POST['disconnect'])) {
    session_destroy();
    header("Location: signin.php");
}
if (!isset($_SESSION['name'])) {
    header("Location: signin.php");
}

if (isset($_POST['delete_def'])) {
    $id_user = $_SESSION['id'];
    $passwd = $_POST['passwd'];
    $res = $mysqli->query("SELECT * FROM users WHERE id_user = '$id_user'");
    $user = mysqli_fetch_assoc($res);

    if (password_verify($passwd, $user['password'])) {
        $mysqli->query("DELETE FROM users WHERE id_user = '$id_user'");
        $mysqli->query("DELETE FROM user_projects WHERE id_user  = '$id_user'");
        session_destroy();
        header("Location: signin.php");
    } else {
        echo "Mot de passe erroné";
    }
}

?>
<div class="back"><p>Hello <?= $_SESSION['name'] ?> <?= $_SESSION['lastname'] ?></p></div>
<div class="back"><p>Rol: <?= $_SESSION['rol'] ?></p></div>
<main>
    <div class="add-button"><a href="add.php" class="add-project">Add project</a></div>
    <form method="POST">
    <table class="projects-table">
        <tr>
            <th>Projects</th>
            <th>Actions</th>
        </tr>
        <?php
        if ($_SESSION['rol'] === "admin" || $_SESSION['rol'] === "client") {
            $res = $mysqli->query("SELECT * FROM projects");
        } elseif ($_SESSION['rol'] === "dev") {
            $id = $_SESSION['id'];
            $res = $mysqli->query("SELECT * FROM users JOIN user_projects ON user_projects.id_user = users.id_user JOIN projects ON projects.id = user_projects.id WHERE user_projects.id_user = '$id'");
        }
        foreach ($res as $result) {
            ?>
            <tr>
                <td>
                    <div id="<?= $result['id'] ?>"><?= $result['title'] ?></div>
                </td>
                <td>
                    <a href="project.php?id_unic=<?= $result['id'] ?>" class="see-project">See</a>
                    <a href="index.php?mod=<?= $result['id'] ?>" class="modify-project">Modify</a>
                    <a href="assign.php?id=<?= $result['id'] ?>" class="assign-project">Assign</a>
                    <a href="index.php?id=<?= $result['id'] ?>" class="delete-project">Delete</a>
                </td>
            </tr>
            <?php
        }
        ?>
    </form>
    </table>
</main>
<form method="POST">
    <input type="submit" name="disconnect" value="Disconnect">
    <input type="submit" value="Delete account" name="delete_account">
</form>
<?php
if (isset($_POST['delete_account'])) {
    ?>
    <form action="" method="POST">
        <label for="passwd">Introduce your password to delete your account</label><br>
        <input type="password" name="passwd" id="passwd"><br>
        <input type="submit" value="Delete" name="delete_def">
    </form>
    <?php
}
?>
<br><br><br><br>
<script>
    <?php
    if(isset($_GET['mod'])){
    $id = $_GET['mod'];
    $row = $mysqli->query("SELECT * FROM projects WHERE id='$id'");
    $respons = mysqli_fetch_assoc($row);
    $title = $respons['title'];
    ?>
    const e = document.getElementById("<?php echo $_GET['mod']?>")
    e.innerHTML = "<form method='POST'><input type='text' name='new_title' value='<?= $title ?>'><input type='submit' value='modif' name='modif_title'></form>";
    <?php
    }
    if($_SESSION['rol'] === "client" || $_SESSION['rol'] === "dev"){
    ?>
    const modif = [...document.querySelectorAll('.modify-project')]
    const del = [...document.querySelectorAll('.delete-project')]
    const assign = [...document.querySelectorAll('.assign-project')]
    assign.forEach(e => e.style.display = "none")
    modif.forEach(e => e.style.display = "none")
    del.forEach(e => e.style.display = "none")
    <?php
    }

    if($_SESSION['rol'] === "dev"){
    ?>
    const add = [...document.querySelectorAll(".add-project")]
    add.forEach(e => e.style.display = "none")
    <?php
    }
    ?>
</script>
<?php include "./../components/footer.php" ?>
