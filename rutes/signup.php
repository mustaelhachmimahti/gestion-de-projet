<?php session_start();
if(isset( $_SESSION['name'] )){
    header("Location: index.php");
}
?>
<?php include "./../components/connection.php" ?>
<?php include "./../components/header.php" ?>

    <main>
        <div class="sign">
            <form method="POST">
                <div>
                    <input type="text" name="name" placeholder="First Name"><br>
                    <input type="text" name="lastname" placeholder="Last Name"><br>
                    <input type="email" name="email" placeholder="Email"><br>
                    <input type="password" name="password" placeholder="Password"><br>
                    <input type="password" name="password_two" placeholder="Confirm password"><br>
                    <label for="dev">Dev</label>
                    <input type="radio" name="rol" id="dev" value="dev">
                    <label for="client">Customer</label>
                    <input type="radio" name="rol" id="client" value="client"><br>
                    <input type="submit" name="sign_in" value="Sign In">
                </div>
            </form>
        </div>
    </main>

<?php
if (isset($_POST['sign_in'])) {
    $errors = [];
    $name = $_POST['name'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $password_two = $_POST['password_two'];
    $rol = $_POST['rol'];
    if ($password !== $password_two) {
        array_push($errors, "Is not the same password");
    }
    if (empty($password)) {
        array_push($errors, "Password required");
    }else{
        if (strlen($password) < 6) {
            array_push($errors, "Password to short");
        }
    }
    if (empty($email)) {
        array_push($errors, "Email required");
    }
    if (empty($name)) {
        array_push($errors, "First name required");
    }
    if (empty($lastname)) {
        array_push($errors, "Last name required");
    }

    if (!empty($errors)) {
        foreach ($errors as $foo) {
           ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Ups!!</strong> <?= $foo ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
                <?php
        }
    } else {
            $options = [
                'cost' => 12
            ];
            $password_hashed = password_hash($password, PASSWORD_BCRYPT, $options);
            $mysqli->query("INSERT INTO users (email, first_name, last_name, password, rol) VALUES ('$email','$name','$lastname','$password_hashed', '$rol')");
            header("Location: signin.php");
    }
}
?>

<?php include "./../components/footer.php" ?>