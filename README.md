# Gestion de Projet

## Fonctionnement

Dans ce projet, nous avons trois rôles principaux.

### Admin

Nous avons le rôle d'administrateur, qui est déjà prédéfini dans la base de données.

Courriel : `admin@admin.com`<br>
Mot de passe: `adminadmin`

J'ai créé ce super utilisateur manuellement dans la base de données afin que n'importe quel utilisateur ne puisse pas
avoir ces privilèges.

Cet utilisateur peut visualiser, ajouter, modifier, supprimer et assigner des projets. Et il peut également désassigner un
projet.

Il peux uniquement assigner des projets aux utilisateurs qui sont des développeurs (`dev`).

Un petit rappel:

Pour modifier uniquement le titre, cliquez sur le bouton `Modify`, mais si vous voulez modifier le titre et le contenu,
cliquez sur le bouton `See`, si vous êtes un administrateur vous verrez le bouton `Modify`.

### Dev

Le dev est la personne qui va développer le projet en question.

Sur la page d'accueil de `dev`, Il ne verra que les projets auxquelles il a êtes assigne.

Vous n'avez le droit de voir que le projet sur lequel vous allez travailler, car l'administrateur vous aura assigner à
ce projet. Si vous n'êtes assigner à aucun projet, vous ne verrez rien.

Si vous êtes assigne à un projet avec d'autres personnes, vous pourrez voir qui est assigne à ce projet.
### Client

Le client est un peu comme le dev, avec pour seule option la possibilité d'ajouter un nouveau projet.

## A améliorer

Sans compter la sécurité, si vous trouvez des bugs, des choses à améliorer, des propositions concernant les
fonctionnalités de ce projet, j'aimerais en être informé