<?php session_start() ?>

<?php include "./../components/connection.php"; ?>
<?php
if (!isset($_SESSION['name'])) {
    header("Location: signin.php");
}
?>

<?php include "./../components/header.php"; ?>

<main>
    <form method="POST">
        <div class="add-project">
            <div>
                <label for="title">Name of project</label><br><br>
                <input type="text" name="title" id="title">
            </div>
            <br>
            <div>
                <label for="description">Description</label><br><br>
                <textarea id="description" name="description" rows="7" cols="40"></textarea>
            </div>
            <input type="submit" name="add_it" value="Register">
        </div>
    </form>
    <div class="back"><a href="./index.php"><< Return</a></div>
</main>

<?php
if (isset($_POST['add_it'])) {
    $title = $_POST['title'];
    $description = $_POST['description'];
    $mysqli->query("INSERT INTO projects (title, description) VALUES ('$title', '$description')");
}
?>

<?php include "./../components/footer.php"; ?>
