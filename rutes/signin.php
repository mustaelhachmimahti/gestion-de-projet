<?php session_start();
if (isset($_SESSION['name'])) {
    header("Location: index.php");
}
?>
<?php include "./../components/connection.php" ?>
<?php include "./../components/header.php" ?>


    <main>
        <div class="sign">
            <form method="POST">
                <div>
                    <input type="email" name="email" placeholder="Email"><br>
                    <input type="password" name="password" placeholder="Password"><br>
                    <input type="submit" name="sign_in" value="Sign In">
                    <a href="./signup.php">Sign Up</a>
                </div>
            </form>
        </div>
    </main>


<?php
if (isset($_POST['sign_in'])) {
    $errors = [];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $res = $mysqli->query("SELECT * FROM users WHERE email = '$email'");
    $user = mysqli_fetch_assoc($res);
    $rows = $res->num_rows;


    if (empty($email)) {
        array_push($errors, "Email required");
    } else {
        if (!$rows) {
            array_push($errors, "Email doesn't exist, register you before yo log in");
        }
    }
    if (empty($password)) {
        array_push($errors, "Password required");
    }
    if (!empty($errors)) {
        foreach ($errors as $err) {
           ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Ups!!</strong> <?= $err ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
                <?php
        }
    } else {
        if ($rows) {
            if (password_verify($password, $user['password'])) {
                $_SESSION['name'] = $user['first_name'];
                $_SESSION['lastname'] = $user['last_name'];
                $_SESSION['rol'] = $user['rol'];
                $_SESSION['id'] = $user['id_user'];
                header("Location: index.php");
            } else {
                echo "Sorry, your password or your email was incorrect";
            }
        }
    }
}
?>

<?php include "./../components/footer.php" ?>